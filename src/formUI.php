<?php

namespace CenarioDev\formUI;

/**
 * Criar o componente de input.
 * Para pergar o retono do status de todos os inputs, utilize a função ***inputValidateValue()***,
 * armazenando em uma variavel o retorno ***true*** ou ***false***.
 * Exemplo: const status = inputValidateValue().
 * Caso seu Form tenha um input do tipo CEP, passe o id dentro da função inputValidateValue(''), na função de submit
 * @param classContainer: Estilizar div global, contendo todos os elementos.
 * @param classContent: Estilizar div, contendo o input e o icone.
 * @param classInput: Estilizar o input.
 * @param classIconError: Estilizar o icone.
 * @param classTextError: Estilizar o texto de erro.
 * @param typeValidate: 'nome' | 'cpf' | 'cnpj' | 'cpfcnpj' | 'datanasc' | 'email' | 'cel' | 'tel' | 
 * 'celreq' | 'telreq' | 'telreq' | 'telcel' | 'cep' | 'required' | 'banks' | 
 * 'cidade-nascimento' | 'numbers' | 'datExpedicao' | 'datNascimentoEmprestimo' | 
 * 'renda-mensal' | 'parcelas' | 'confirma-senha' | 'senha' | 'null'.
 * @param 
 */

function Input(
  $props = array(
    'id' => '',
    'placeholder' => '',
    'typeValidate' => '',
    'typeInput' => '',
    'textError' => '',
    'classContainer' => '',
    'classContent' => '',
    'classInput' => '',
    'classIconError' => '',
    'classTextError' => '',
    'iconLittleFace' => true
  )
) {
  $displayIconLittleFace = isset($props['iconLittleFace']) && empty($props['iconLittleFace']) ? 'none' : 'flex';
?>

  <span id="status-input-<?php echo $props['id'] ?>" style="display: none;"></span>

  <div class="container-dynamic-input <?php echo $props['classContainer'] ?>">
    <div class="content-dynamic-input <?php echo $props['classContent'] ?>">
      <input id="<?php echo $props['id'] ?>" class="<?php echo $props['classInput'] ?> input-dynamic-input" type="<?php echo $props['typeInput'] ?>" name="<?php echo $props['typeValidate'] ?>" placeholder="<?php echo $props['placeholder'] ?>" oninput="inputValidateValue('<?php echo $props['id'] ?>'); maskInput('<?php echo $props['id'] ?>', '<?php echo $props['typeValidate'] ?>');
          maxLengthInput('<?php echo $props['id'] ?>', '<?php echo $props['typeValidate'] ?>'
        )
        ">
      <!-- Carinha -->

      <div id="spiner-dynamic-input-<?php echo $props['id'] ?>" class="spiner-dynamic-input invisible"></div>

      <div style="display: <?php echo $displayIconLittleFace ?>;">
        <svg id="icon-status-dynamic-input-default-<?php echo $props['id'] ?>" class="icon-error-dynamic-input  <?php echo $props['classIconError'] ?>" width="22" height="22" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M18 10C18 7.87827 17.1571 5.84344 15.6569 4.34315C14.1566 2.84285 12.1217 2 10 2C7.87827 2 5.84344 2.84285 4.34315 4.34315C2.84285 5.84344 2 7.87827 2 10C2 12.1217 2.84285 14.1566 4.34315 15.6569C5.84344 17.1571 7.87827 18 10 18C12.1217 18 14.1566 17.1571 15.6569 15.6569C17.1571 14.1566 18 12.1217 18 10ZM20 10C20 12.6522 18.9464 15.1957 17.0711 17.0711C15.1957 18.9464 12.6522 20 10 20C8.68678 20 7.38642 19.7413 6.17317 19.2388C4.95991 18.7362 3.85752 17.9997 2.92893 17.0711C1.05357 15.1957 0 12.6522 0 10C0 7.34784 1.05357 4.8043 2.92893 2.92893C4.8043 1.05357 7.34784 0 10 0C11.3132 0 12.6136 0.258658 13.8268 0.761205C15.0401 1.26375 16.1425 2.00035 17.0711 2.92893C17.9997 3.85752 18.7362 4.95991 19.2388 6.17317C19.7413 7.38642 20 8.68678 20 10ZM8 7.5C8 8.3 7.3 9 6.5 9C5.7 9 5 8.3 5 7.5C5 6.7 5.7 6 6.5 6C7.3 6 8 6.7 8 7.5ZM15 7.5C15 8.3 14.3 9 13.5 9C12.7 9 12 8.3 12 7.5C12 6.7 12.7 6 13.5 6C14.3 6 15 6.7 15 7.5ZM10 15.23C8.25 15.23 6.71 14.5 5.81 13.42L7.23 12C7.68 12.72 8.75 13.23 10 13.23C11.25 13.23 12.32 12.72 12.77 12L14.19 13.42C13.29 14.5 11.75 15.23 10 15.23Z" fill="#E7E7E7" />
        </svg>
      </div>

      <!-- Carinha Feliz -->
      <div style="display: <?php echo $displayIconLittleFace ?>;">
        <svg id="icon-status-dynamic-input-feliz-<?php echo $props['id'] ?>" class="icon-error-dynamic-input invisible  <?php echo $props['classIconError'] ?>" width="22" height="22" viewBox="0 0 20 20" fill="#27ae60" xmlns="http://www.w3.org/2000/svg">
          <path d="M10 0C4.486 0 0 4.486 0 10C0 15.514 4.486 20 10 20C15.514 20 20 15.514 20 10C20 4.486 15.514 0 10 0ZM10 18C5.589 18 2 14.411 2 10C2 5.589 5.589 2 10 2C14.411 2 18 5.589 18 10C18 14.411 14.411 18 10 18Z" />
          <path d="M6.5 9C7.32843 9 8 8.32843 8 7.5C8 6.67157 7.32843 6 6.5 6C5.67157 6 5 6.67157 5 7.5C5 8.32843 5.67157 9 6.5 9Z" />
          <path d="M13.493 8.986C14.3176 8.986 14.986 8.31756 14.986 7.493C14.986 6.66844 14.3176 6 13.493 6C12.6684 6 12 6.66844 12 7.493C12 8.31756 12.6684 8.986 13.493 8.986Z" />
          <path d="M10 16C15 16 16 11 16 11H4C4 11 5 16 10 16Z" />
        </svg>
      </div>


      <!-- CArinha Triste -->
      <div style="display: <?php echo $displayIconLittleFace ?>;">
        <svg id="icon-status-dynamic-input-triste-<?php echo $props['id'] ?>" class="icon-error-dynamic-input invisible <?php echo $props['classIconError'] ?>" width="22" height="22" viewBox="0 0 20 20" fill="#ff4d4d" xmlns="http://www.w3.org/2000/svg">
          <path d="M18 10C18 7.87827 17.1571 5.84344 15.6569 4.34315C14.1566 2.84285 12.1217 2 10 2C7.87827 2 5.84344 2.84285 4.34315 4.34315C2.84285 5.84344 2 7.87827 2 10C2 12.1217 2.84285 14.1566 4.34315 15.6569C5.84344 17.1571 7.87827 18 10 18C12.1217 18 14.1566 17.1571 15.6569 15.6569C17.1571 14.1566 18 12.1217 18 10ZM20 10C20 12.6522 18.9464 15.1957 17.0711 17.0711C15.1957 18.9464 12.6522 20 10 20C8.68678 20 7.38642 19.7413 6.17317 19.2388C4.95991 18.7362 3.85752 17.9997 2.92893 17.0711C1.05357 15.1957 0 12.6522 0 10C0 7.34784 1.05357 4.8043 2.92893 2.92893C4.8043 1.05357 7.34784 0 10 0C11.3132 0 12.6136 0.258658 13.8268 0.761205C15.0401 1.26375 16.1425 2.00035 17.0711 2.92893C17.9997 3.85752 18.7362 4.95991 19.2388 6.17317C19.7413 7.38642 20 8.68678 20 10ZM8 7.5C8 8.3 7.3 9 6.5 9C5.7 9 5 8.3 5 7.5C5 6.7 5.7 6 6.5 6C7.3 6 8 6.7 8 7.5ZM15 7.5C15 8.3 14.3 9 13.5 9C12.7 9 12 8.3 12 7.5C12 6.7 12.7 6 13.5 6C14.3 6 15 6.7 15 7.5Z" />
          <path d="M14.1901 13.81C13.2901 12.73 11.7501 12 10.0001 12C8.25006 12 6.71006 12.73 5.81006 13.81L7.23006 15.23C7.68006 14.51 8.75006 14 10.0001 14C11.2501 14 12.3201 14.51 12.7701 15.23L14.1901 13.81Z" />
        </svg>
      </div>

    </div>
    <p id="text-error-dynamic-input-<?php echo $props['id'] ?>" class="text-error-dynamic-input noVisibled <?php echo $props['classTextError'] ?>">
      <?php echo $props['textError'] ?>
    </p>


    <!-- hidden -->
    <?php
    if ($props['typeValidate'] === 'cep') {
      $inputID = $props['id'];

      echo "
          <input class='' id='rIdEstados-$inputID'  type='text' name='rIdEstados-$inputID' hidden />
          <input class='' id='rIdCidades-$inputID'  name='rIdCidades-$inputID' value='' hidden />
          <input class='' id='ddd-Cidades-$inputID'  name='ddd-Cidades-$inputID'  value='' hidden />
        ";
    };
    ?>
  </div>
<?php
}
?>


<!-- SelectBox -->
<?php
/**
 * Criar o componente de Select.
 * Para pergar o retono do status de todos os inputs, utilize a função ***inputValidateValue()***,
 * armazenando em uma variavel o retorno ***true*** ou ***false***.
 * Exemplo: const status = inputValidateValue().
 * 
 * @param options: Deve receber um array de objetos, onde cada item tenha uma chave ***label*** e ***value***.
 * @param classDisplayContainer: Estilizar container do select (tag select e icone).
 * @param classArrowIcon: Estiliza o icone do display, podendo ajustar a posição do icone.
 * @param classSelect: Estiliza a tag do select.
 * @param classOption: Estiliza cada option.
 * @param classTextError: Estiliza o texto de erro
 */


function Select(
  $options = array(
    array('label' => 'Label1', 'value' => 'label1', 'status' => false),
  ),

  $props = array(
    'id' => '',
    'colorIcon' => '#000',
    'colorText' => '#000',
    'colorPlaceholder' => '#BCBCBC',
    'textError' => 'Campo ínvalido',
    'classDisplayContainer' => '',
    'clasContainerIcon' => '',
    'classArrowIcon' => '',
    'classSelect' => '',
    'classOption' => '',
    'classTextError' => '',
    'iconLittleFace' => false
  )
) {

  $displayIconLittleFace = isset($props['iconLittleFace']) && empty($props['iconLittleFace']) ? 'none' : 'flex';
?>

  <div class="container-select-dynamic-select">
    <div class="display-dynamic-select <?php echo $props['classDisplayContainer'] ?>">
      <select style="z-index: 999;" name="required" id="<?php echo $props['id'] ?>" class="input-dynamic-input select-dynamic-select <?php echo $props['classSelect'] ?>" onchange="inputValidateValue(); custonSelect('<?php echo $props['id'] ?>', '<?php echo $props['colorText'] ?>', '<?php echo $props['colorPlaceholder'] ?>')">
        <?php
        foreach ($options as $option) {
        ?>
          <option style="color: <?php echo $props['colorText'] ?>;" class="<?php echo $props['classOption'] ?>" value="<?php echo $option['value'] ?>">
            <?php echo $option['label'] ?>
          </option>

        <?php
        }
        ?>
      </select>
      <div class="icons-dynamic-select <?php echo $props['clasContainerIcon'] ?>">
        <div class="icon-face-dynamic-select icon-arraw-dinamic-select">
          <svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg" class="<?php echo $props['classArrowIcon'] ?>">
            <path d="M5.23748 7.47826L0.55106 2.95652C-0.0197223 2.4058 -0.147097 1.77565 0.168937 1.06609C0.48497 0.356522 1.04794 0.00115942 1.85785 0H11.1406C11.9517 0 12.5153 0.355362 12.8313 1.06609C13.1473 1.77681 13.0194 2.40696 12.4474 2.95652L7.76094 7.47826C7.5807 7.65217 7.38543 7.78261 7.17514 7.86956C6.96485 7.95652 6.73954 8 6.49921 8C6.25888 8 6.03358 7.95652 5.82329 7.86956C5.613 7.78261 5.41773 7.65217 5.23748 7.47826Z" fill="<?php echo $props['colorIcon'] ?>" />
          </svg>
        </div>

        <!-- Carinha -->
        <div style="display: none;" class="">
          <svg id="icon-status-dynamic-input-default-<?php echo $props['id'] ?>" class="icon-error-dynamic-input   <?php echo $props['classIconError'] ?>" width="22" height="22" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M18 10C18 7.87827 17.1571 5.84344 15.6569 4.34315C14.1566 2.84285 12.1217 2 10 2C7.87827 2 5.84344 2.84285 4.34315 4.34315C2.84285 5.84344 2 7.87827 2 10C2 12.1217 2.84285 14.1566 4.34315 15.6569C5.84344 17.1571 7.87827 18 10 18C12.1217 18 14.1566 17.1571 15.6569 15.6569C17.1571 14.1566 18 12.1217 18 10ZM20 10C20 12.6522 18.9464 15.1957 17.0711 17.0711C15.1957 18.9464 12.6522 20 10 20C8.68678 20 7.38642 19.7413 6.17317 19.2388C4.95991 18.7362 3.85752 17.9997 2.92893 17.0711C1.05357 15.1957 0 12.6522 0 10C0 7.34784 1.05357 4.8043 2.92893 2.92893C4.8043 1.05357 7.34784 0 10 0C11.3132 0 12.6136 0.258658 13.8268 0.761205C15.0401 1.26375 16.1425 2.00035 17.0711 2.92893C17.9997 3.85752 18.7362 4.95991 19.2388 6.17317C19.7413 7.38642 20 8.68678 20 10ZM8 7.5C8 8.3 7.3 9 6.5 9C5.7 9 5 8.3 5 7.5C5 6.7 5.7 6 6.5 6C7.3 6 8 6.7 8 7.5ZM15 7.5C15 8.3 14.3 9 13.5 9C12.7 9 12 8.3 12 7.5C12 6.7 12.7 6 13.5 6C14.3 6 15 6.7 15 7.5ZM10 15.23C8.25 15.23 6.71 14.5 5.81 13.42L7.23 12C7.68 12.72 8.75 13.23 10 13.23C11.25 13.23 12.32 12.72 12.77 12L14.19 13.42C13.29 14.5 11.75 15.23 10 15.23Z" fill="#E7E7E7" />
          </svg>
        </div>

        <!-- Carinha Feliz -->
        <div style="display: <?php echo $displayIconLittleFace ?>;" class="<?php echo $displayIconLittleFace ? 'icon-face-dynamic-select' : '' ?> icon-face-dynamic-select">
          <svg id="icon-status-dynamic-input-feliz-<?php echo $props['id'] ?>" class="icon-error-dynamic-input  invisible  <?php echo $props['classIconError'] ?>" width="22" height="22" viewBox="0 0 20 20" fill="#27ae60" xmlns="http://www.w3.org/2000/svg">
            <path d="M10 0C4.486 0 0 4.486 0 10C0 15.514 4.486 20 10 20C15.514 20 20 15.514 20 10C20 4.486 15.514 0 10 0ZM10 18C5.589 18 2 14.411 2 10C2 5.589 5.589 2 10 2C14.411 2 18 5.589 18 10C18 14.411 14.411 18 10 18Z" />
            <path d="M6.5 9C7.32843 9 8 8.32843 8 7.5C8 6.67157 7.32843 6 6.5 6C5.67157 6 5 6.67157 5 7.5C5 8.32843 5.67157 9 6.5 9Z" />
            <path d="M13.493 8.986C14.3176 8.986 14.986 8.31756 14.986 7.493C14.986 6.66844 14.3176 6 13.493 6C12.6684 6 12 6.66844 12 7.493C12 8.31756 12.6684 8.986 13.493 8.986Z" />
            <path d="M10 16C15 16 16 11 16 11H4C4 11 5 16 10 16Z" />
          </svg>
        </div>


        <!-- CArinha Triste -->
        <div style="display: <?php echo $displayIconLittleFace ?>;" class="<?php echo $displayIconLittleFace ? 'icon-face-dynamic-select' : '' ?> icon-face-dynamic-select">
          <svg id="icon-status-dynamic-input-triste-<?php echo $props['id'] ?>" class="icon-error-dynamic-input  invisible <?php echo $props['classIconError'] ?>" width="22" height="22" viewBox="0 0 20 20" fill="#ff4d4d" xmlns="http://www.w3.org/2000/svg">
            <path d="M18 10C18 7.87827 17.1571 5.84344 15.6569 4.34315C14.1566 2.84285 12.1217 2 10 2C7.87827 2 5.84344 2.84285 4.34315 4.34315C2.84285 5.84344 2 7.87827 2 10C2 12.1217 2.84285 14.1566 4.34315 15.6569C5.84344 17.1571 7.87827 18 10 18C12.1217 18 14.1566 17.1571 15.6569 15.6569C17.1571 14.1566 18 12.1217 18 10ZM20 10C20 12.6522 18.9464 15.1957 17.0711 17.0711C15.1957 18.9464 12.6522 20 10 20C8.68678 20 7.38642 19.7413 6.17317 19.2388C4.95991 18.7362 3.85752 17.9997 2.92893 17.0711C1.05357 15.1957 0 12.6522 0 10C0 7.34784 1.05357 4.8043 2.92893 2.92893C4.8043 1.05357 7.34784 0 10 0C11.3132 0 12.6136 0.258658 13.8268 0.761205C15.0401 1.26375 16.1425 2.00035 17.0711 2.92893C17.9997 3.85752 18.7362 4.95991 19.2388 6.17317C19.7413 7.38642 20 8.68678 20 10ZM8 7.5C8 8.3 7.3 9 6.5 9C5.7 9 5 8.3 5 7.5C5 6.7 5.7 6 6.5 6C7.3 6 8 6.7 8 7.5ZM15 7.5C15 8.3 14.3 9 13.5 9C12.7 9 12 8.3 12 7.5C12 6.7 12.7 6 13.5 6C14.3 6 15 6.7 15 7.5Z" />
            <path d="M14.1901 13.81C13.2901 12.73 11.7501 12 10.0001 12C8.25006 12 6.71006 12.73 5.81006 13.81L7.23006 15.23C7.68006 14.51 8.75006 14 10.0001 14C11.2501 14 12.3201 14.51 12.7701 15.23L14.1901 13.81Z" />
          </svg>
        </div>

      </div>

    </div>
    <p id="text-error-dynamic-input-<?php echo $props['id'] ?>" class="text-error-dynamic-input noVisibled <?php echo $props['classTextError'] ?>">
      <?php echo $props['textError'] ?>
    </p>
  </div>
<?php
}
?>

<!-- Checkbox -->
<?php
/**
 * Criar o componente de Checkbox.
 * Para pergar o valor selecionado no checkbox, utilize o getElementById pelo id ***"status-checked-dynamic-select-ID"***,
 * onde esse ***'ID'*** e o mesmo que foi passado como parametro.
 * 
 * @param label: A label deve ser passada como tag customizada em formato de string, seguindo o exemplo:.
 * <label class="" for="checkTermos">Lorem ipsun</label>.
 * @param functionGetStatusValues: Seta a função de validação pelo metodo onInput.
 * Deve ser declarado, quando há a necessidade de capturar o status de todos os campos de forma externa.
 * Exemplo: Habilitar um botão de "ENVIAR"
 * @param 

 */

function Checkbox(
  $props = array(
    'id' => '',
    'label' => '',
    'checked' => 'checked',
    'textError' => 'Campo ínvalido',
    'classTextError' => '',
    'iconLittleFace' => false
  )
) {
  $isCkecked = $props['checked'] == true ? 'true' : 'false';

  $isVisibledIconFeliz = $props['checked'] == true ? '' : 'invisible';
  $displayIconLittleFace = isset($props['iconLittleFace']) && empty($props['iconLittleFace']) ? 'none' : 'block';
?>

  <div class="container-dynamic-termos">
    <span id="status-checked-dynamic-select-<?php echo $props['id'] ?>" style="display: none;">
      <?php echo $isCkecked ?>
    </span>
    <div class="content-dynamic-termos">
      <input type="checkbox" id="<?php echo $props['id'] ?>" class="input-dynamic-input custon-input-checked" name="required" onclick="inputValidateValue(); setCarinhaCheck('<?php echo $props['id'] ?>')" <?php echo $props['checked'] ?>>
      <?php
      echo $props['label']
      ?>
      <!-- Carinha Feliz -->
      <div style="display: <?php echo $displayIconLittleFace ?>;">
        <svg id="icon-status-dynamic-checked-feliz-<?php echo $props['id'] ?>" class=" icon-error-dynamic-input <?php echo $isVisibledIconFeliz ?>" width="22" height="22" viewBox="0 0 20 20" fill="#27ae60" xmlns="http://www.w3.org/2000/svg">
          <path d="M10 0C4.486 0 0 4.486 0 10C0 15.514 4.486 20 10 20C15.514 20 20 15.514 20 10C20 4.486 15.514 0 10 0ZM10 18C5.589 18 2 14.411 2 10C2 5.589 5.589 2 10 2C14.411 2 18 5.589 18 10C18 14.411 14.411 18 10 18Z" />
          <path d="M6.5 9C7.32843 9 8 8.32843 8 7.5C8 6.67157 7.32843 6 6.5 6C5.67157 6 5 6.67157 5 7.5C5 8.32843 5.67157 9 6.5 9Z" />
          <path d="M13.493 8.986C14.3176 8.986 14.986 8.31756 14.986 7.493C14.986 6.66844 14.3176 6 13.493 6C12.6684 6 12 6.66844 12 7.493C12 8.31756 12.6684 8.986 13.493 8.986Z" />
          <path d="M10 16C15 16 16 11 16 11H4C4 11 5 16 10 16Z" />
        </svg>
      </div>

      <!-- CArinha Triste -->
      <div style="display: <?php echo $displayIconLittleFace ?>;">
        <svg id="icon-status-dynamic-checked-triste-<?php echo $props['id'] ?>" class=" icon-error-dynamic-input invisible" width="22" height="22" viewBox="0 0 20 20" fill="#ff4d4d" xmlns="http://www.w3.org/2000/svg">
          <path d="M18 10C18 7.87827 17.1571 5.84344 15.6569 4.34315C14.1566 2.84285 12.1217 2 10 2C7.87827 2 5.84344 2.84285 4.34315 4.34315C2.84285 5.84344 2 7.87827 2 10C2 12.1217 2.84285 14.1566 4.34315 15.6569C5.84344 17.1571 7.87827 18 10 18C12.1217 18 14.1566 17.1571 15.6569 15.6569C17.1571 14.1566 18 12.1217 18 10ZM20 10C20 12.6522 18.9464 15.1957 17.0711 17.0711C15.1957 18.9464 12.6522 20 10 20C8.68678 20 7.38642 19.7413 6.17317 19.2388C4.95991 18.7362 3.85752 17.9997 2.92893 17.0711C1.05357 15.1957 0 12.6522 0 10C0 7.34784 1.05357 4.8043 2.92893 2.92893C4.8043 1.05357 7.34784 0 10 0C11.3132 0 12.6136 0.258658 13.8268 0.761205C15.0401 1.26375 16.1425 2.00035 17.0711 2.92893C17.9997 3.85752 18.7362 4.95991 19.2388 6.17317C19.7413 7.38642 20 8.68678 20 10ZM8 7.5C8 8.3 7.3 9 6.5 9C5.7 9 5 8.3 5 7.5C5 6.7 5.7 6 6.5 6C7.3 6 8 6.7 8 7.5ZM15 7.5C15 8.3 14.3 9 13.5 9C12.7 9 12 8.3 12 7.5C12 6.7 12.7 6 13.5 6C14.3 6 15 6.7 15 7.5Z" />
          <path d="M14.1901 13.81C13.2901 12.73 11.7501 12 10.0001 12C8.25006 12 6.71006 12.73 5.81006 13.81L7.23006 15.23C7.68006 14.51 8.75006 14 10.0001 14C11.2501 14 12.3201 14.51 12.7701 15.23L14.1901 13.81Z" />
        </svg>
      </div>
    </div>
    <p id="text-error-dynamic-input-<?php echo $props['id'] ?>" class="text-error-dynamic-input noVisibled <?php echo $props['classTextError'] ?>">
      <?php echo $props['textError'] ?>
    </p>
  </div>

<?php
}
?>

<!-- RadioRestriction -->
<?php
/**
 * Criar o componente de YesNo.
 * Para pergar o valor selecionado do component, utilize o getElementById pelo id ***"checked-ID"***,
 * onde esse ***'ID'*** e o mesmo que foi passado como parametro.
 * Será retornado ***true(SIM)*** ou ***false(NAO)***
 * Exemplo: const checked = document.getElementById('checked-MEUID').checked
 * 
 * @param label: A label deve ser passada como tag customizada em formato de string, seguindo o exemplo:.
 * <label class="" for="checkTermos">Lorem ipsun</label>.
 * @param
 * @param 

 */

function RadioRestriction(
  $props = array(
    'id' => '',
    'textError' => 'Campo ínvalido',
    'bgColor' => '#fff',
    'color' => '#bcbcbc',
    'bgColorHoverButton' => '#f88430',
    'colorTextHoverButton' => '#fff',
    'borderColor'  => 'gray',
    'classContentButton' => '',
    'classButton' => '',
    'classTextError' => ''
  )
) {
?>


  <div class="container-dynamic-button">
    <input name="required" id="checked-<?php echo $props['id'] ?>" style="display: none;" type="checkbox"> <!-- salva true(SIM) ou false(NAO) -->
    <input class="input-dynamic-input" style="display: none; " id="<?php echo $props['id'] ?>" type="checkbox">

    <div class="content-dynamic-button <?php echo $props['classContentButton'] ?>">
      <button id="btn-label-yes-dynamic-button-<?php echo $props['id'] ?>" type="button" style="background-color: <?php echo $props['bgColor'] ?>; color: <?php echo $props['color'] ?>; border: 1px solid <?php echo $props['borderColor'] ?>;" class="button-dynamic-button <?php echo $props['classButton'] ?>" onclick="
          setValueYesNo(
            'SIM', 
            '<?php echo $props['id'] ?>', 
            '<?php echo $props['bgColorHoverButton'] ?>', 
            '<?php echo $props['colorTextHoverButton'] ?>',
            '<?php echo $props['bgColor'] ?>',
            '<?php echo $props['color'] ?>'
          );
          inputValidateValue()
        ">
        Sim
      </button>
      <button id="btn-label-no-dynamic-button-<?php echo $props['id'] ?>" type="button" style="background-color: <?php echo $props['bgColor'] ?>; color: <?php echo $props['color'] ?>; border: 1px solid <?php echo $props['borderColor'] ?>;" class="button-dynamic-button <?php echo $props['classButton'] ?>" onclick="
          setValueYesNo(
            'NAO', 
            '<?php echo $props['id'] ?>', 
            '<?php echo $props['bgColorHoverButton'] ?>', 
            '<?php echo $props['colorTextHoverButton'] ?>',
            '<?php echo $props['bgColor'] ?>',
            '<?php echo $props['color'] ?>'
          );
          inputValidateValue()
        ">
        Não
      </button>
    </div>
    <p id="text-error-dynamic-input-<?php echo $props['id'] ?>" class="text-error-dynamic-input noVisibled <?php echo $props['classTextError'] ?>">
      <?php echo $props['textError'] ?>
    </p>
  </div>

<?php
}
?>

<script async>
  const custonSelect = (e, t, l) => {
    let i = document.getElementById(e),
      s = i.value;
    "" === s ? i.style.color = l : i.style.color = t
  };

  function hasNumbers(e) {
    return /\d/g.test(e)
  }
  const setCarinhaCheck = e => {
      let t = document.getElementById(e),
        l = document.getElementById(`icon-status-dynamic-checked-feliz-${e}`),
        i = document.getElementById(`icon-status-dynamic-checked-triste-${e}`);
      t.checked ? (l.classList.remove("invisible"), i.classList.add("invisible")) : (l.classList.add("invisible"), i.classList.remove("invisible"))
    },
    setValueYesNo = (e, t, l, i, s, d) => {
      let n = document.getElementById(`checked-${t}`),
        r = document.getElementById(t),
        c = document.getElementById(`btn-label-yes-dynamic-button-${t}`),
        o = document.getElementById(`btn-label-no-dynamic-button-${t}`);
      r.checked = !0, "SIM" === e ? (n.checked = !0, c.style.backgroundColor = l, c.style.color = i, c.style.border = 0, o.style.backgroundColor = s, o.style.color = d, o.style.border = "1px solid") : (n.checked = !1, o.style.backgroundColor = l, o.style.color = i, o.style.border = 0, c.style.backgroundColor = s, c.style.color = d, c.style.border = "1px solid")
    },
    validateCheckbox = (e, t, l, i) => {
      document.getElementById(e);
      let s = document.getElementById(t),
        d = document.getElementById(l),
        n = document.getElementById(i);
      "true" == s.innerHTML.trim() ? (s.innerHTML = "false", n.classList.remove("invisible"), d.classList.add("invisible")) : (s.innerHTML = "true", n.classList.add("invisible"), d.classList.remove("invisible"))
    };
  let dataOptions = {},
    itensOptionsToSelect = "";
  const testeMinhaFuncao = () => {
      console.log("Testing...")
    },
    inputValidateValue = e => {
      let t = document.querySelectorAll(".input-dynamic-input"),
        l = document.getElementById("btn-avancar-01"),
        i = document.getElementById("btn-avancar-02"),
        s = 0;
      t.forEach(e => {
        let t = document.getElementById(`text-error-dynamic-input-${e.id}`),
          l = document.getElementById(`icon-status-dynamic-input-feliz-${e.id}`),
          i = document.getElementById(`icon-status-dynamic-input-triste-${e.id}`),
          d = document.getElementById(`icon-status-dynamic-input-default-${e.id}`),
          n = listTypes[e.name];
        if ("cep" != e.name) {
          let r = !1;
          !1 === (r = "checkbox" === e.type ? e.checked : n(e.value)) && (s += 1), d && d.classList.add("invisible"), r ? (d && (l.classList.remove("invisible"), i.classList.add("invisible")), t.classList.add("noVisibled")) : (d && (l.classList.add("invisible"), i.classList.remove("invisible")), t.classList.remove("noVisibled"))
        }
      });
      let d = Array.from(t),
        n = d.filter(e => "cep" == e.name)[0];
      n && "strCep" == e && validateCep(n, s);
      let r = !(s > 0);
      l && (r ? l.classList.remove("disabled") : l.classList.add("disabled")), i && (r ? i.classList.remove("disabled") : i.classList.add("disabled"));
      let c = Array.from(t).map(e => ({
          id: e.id,
          value: e.value
        })),
        o = {};
      for (let m of c) o[m.id] = m.value;
      return {
        isValidate: r,
        data: o
      }
    },
    maskInput = (e, t) => {
      let l = document.getElementById(e),
        i = listTypesMask[t];
      i && (l.value = i(l.value))
    },
    maxLengthInput = (e, t) => {
      let l = document.getElementById(e),
        i = listMaxLength[t];
      i && (l.maxLength = i)
    },
    setMaskPhone = (e = "") => e = (e = (e = e.replace(/\D/g, "")).replace(/^(\d{2})(\d)/g, "($1) $2")).replace(/(\d)(\d{4})$/, "$1-$2"),
    setMaskCPF = (e = "") => e ? e.replace(/\D/g, "").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d{1,2})$/, "$1-$2") : "",
    setMaskCNPJ = e => e = (e = (e = (e = (e = e.replace(/[^\d]/g, "")).replace(/(\d{2})(\d)/, "$1.$2")).replace(/(\d{3})(\d)/, "$1.$2")).replace(/(\d{3})(\d)/, "$1/$2")).replace(/(\d{4})(\d{1,2})/, "$1-$2"),
    setMaskCpfCNPJ = e => 11 === (e = e.replace(/[^\d]/g, "")).length ? e.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4") : 14 === e.length ? e.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, "$1.$2.$3/$4-$5") : e,
    setMaskCEP = e => e.replace(/\D/g, "").replace(/(\d{5})(\d)/, "$1-$2").replace(/(-\d{3})\d+?$/, "$1"),
    setMaskNumbers = e => {
      let t = /[^0-9]/g;
      return e.replace(t, "")
    };

  function maskCurrency(e, t = "pt-BR", l = "BRL") {
    return new Intl.NumberFormat(t, {
      style: "currency",
      currency: l
    }).format(e)
  }
  const setMaskCurrency = e => {
      let t = event.target.value.split("").filter(e => /\d/.test(e)).join("").padStart(3, "0"),
        l = t.slice(0, -2) + "." + t.slice(-2);
      return maskCurrency(l)
    },
    validateName = e => !hasNumbers(e) && /[A-zÀ-ÿ']+\s([A-zÀ-ÿ']\s?)*[A-zÀ-ÿ'][A-zÀ-ÿ']+/g.test(e),
    validateCpf = (e = "") => {
      let t = e;
      if (11 != (t = t.replace(/\D/g, "")).toString().length || /^(\d)\1{10}$/.test(t)) return !1;
      let l = !0;
      return [9, 10].forEach(e => {
        let i = 0,
          s;
        t.split(/(?=)/).splice(0, e).forEach((t, l) => {
          i += Number(t) * (e + 2 - (l + 1))
        }), (s = (s = i % 11) < 2 ? 0 : 11 - s) != t.substring(e, e + 1) && (l = !1)
      }), l
    },
    validateCNPJ = (e = "") => {
      if (14 !== (e = e.replace(/\D/g, "")).length) return !1;
      let t = 0;
      for (let l = 0; l < 12; l++) t += parseInt(e.charAt(l)) * (l < 4 ? 5 - l : 13 - l);
      let i = t % 11 < 2 ? 0 : 11 - t % 11;
      t = 0;
      for (let s = 0; s < 13; s++) t += parseInt(e.charAt(s)) * (s < 5 ? 6 - s : 14 - s);
      let d = t % 11 < 2 ? 0 : 11 - t % 11;
      return i === parseInt(e.charAt(12)) && d === parseInt(e.charAt(13))
    },
    removeMask = e => e.replace(/[^a-zA-Z0-9]/g, ""),
    validateCPFCNPJ = e => {
      let t = removeMask(e);
      return 11 === t.length ? validateCpf(e) : 14 === t.length && validateCNPJ(e)
    },
    validateBirthDate = e => {
      let t = new Date(e);
      if (isNaN(t.getTime())) return !1;
      let l = new Date,
        i = new Date;
      i.setFullYear(l.getFullYear() - 18);
      let s = new Date;
      return s.setFullYear(l.getFullYear() - 120), !(t > l) && !(t < s)
    },
    validateEmail = e => /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(e),
    validateCel = e => {
      let t = e.replace(/[^\d]+/g, "").toString();
      return RegExp("^((1[1-9])|([2-9][0-9]))((3[0-9]{3}[0-9]{4})|(9[0-9]{3}[0-9]{5}))$").test(t)
    },
    validateLandline = e => {
      let t = setMaskPhone(e),
        l = /^\(?([1-9]{2})\)?[-. ]?([2-8]{1})([0-9]{3,4})[-. ]?([0-9]{4})$/,
        i = /^\(?([1-9]{2})\)?[-. ]?([9]{1})([0-9]{3,4})[-. ]?([0-9]{4})$/;
      return l.test(t) && !i.test(t)
    },
    validateAllPhones = e => {
      let t = /^(\(?\d{2}\)?\s)?(\d{4,5}\-\d{4})$/;
      return t.test(e)
    },
    validateCep = (e, t) => {
      let l = document.getElementById(`text-error-dynamic-input-${e.id}`),
        i = document.getElementById(`icon-status-dynamic-input-feliz-${e.id}`),
        s = document.getElementById(`icon-status-dynamic-input-triste-${e.id}`),
        d = document.getElementById(`icon-status-dynamic-input-default-${e.id}`),
        n = document.getElementById(`spiner-dynamic-input-${e.id}`),
        r = document.getElementById(`rIdEstados-${e.id}`),
        c = document.getElementById(`rIdCidades-${e.id}`),
        o = document.getElementById(`ddd-Cidades-${e.id}`);
      if (l.classList.remove("noVisibled"), i.classList.add("invisible"), s.classList.remove("invisible"), d.classList.add("invisible"), !e || !(e.value.length >= 9)) return !1;
      n.classList.remove("invisible"), d.classList.add("invisible"), i.classList.add("invisible"), s.classList.add("invisible"), e.classList.add("disabled-input-dynamic-input"), e.disabled = !0, fetch(`https://api.cep.cenarioconsulta.com.br/logradouro/${e.value}`, {
        method: "GET"
      }).then(e => e.json()).then(m => {
        n.classList.add("invisible"), e.classList.remove("disabled-input-dynamic-input"), e.disabled = !1, 200 != m.code ? (t += 1, s.classList.remove("invisible"), d.classList.add("invisible")) : (l.classList.add("noVisibled"), s.classList.add("invisible"), i.classList.remove("invisible"), c.value = m.body.cidade.rIdCidadesLeads, r.value = m.body.estado.rIdEstadoLeads, setTimeout(() => {
          fetch("https://api.cenarioleads.net.br/api/ddd", {
            method: "POST",
            headers: {
              Accept: "application/json, text/plain, */*",
              Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU3YmQwOThhYzAwYzZmZTZhYjFiZWRlNDFkNWM0ZWFiYzQ0NGU2YzU0NWVhOWNhY2RhZWIyOTlhMDA1NmY3YTM0NWRhMjljOWUyNTMzODE5In0.eyJhdWQiOiIxIiwianRpIjoiZTdiZDA5OGFjMDBjNmZlNmFiMWJlZGU0MWQ1YzRlYWJjNDQ0ZTZjNTQ1ZWE5Y2FjZGFlYjI5OWEwMDU2ZjdhMzQ1ZGEyOWM5ZTI1MzM4MTkiLCJpYXQiOjE1NjY4MjU5OTksIm5iZiI6MTU2NjgyNTk5OSwiZXhwIjoxNjE4NjY1OTk5LCJzdWIiOiIiLCJzY29wZXMiOltdfQ.A2-x_S_TSuFjigSpD8_kdFzlfClyTFSh8XxQ_ZNtZYhOgzo6hIPiJZAOcVaoomJ53cryPlZi5FWrqfArPC-3U6YW4MTvDrNbdZUlQltH9ejMzhWDz-lBW87xQShUY-JgrDrPiA2NI-LAgIydSQwaUXE2mLPrIYfmIX5QDCd2--PWJ_vkfto3AHg8xH7nzy8AEfR4ucjNPaAGZKcOkvXHK7wtTcc9r3dBahTc4NDgQ13xGSkqk8e191r-zU63kQXBGH1nhlH6rbb79lA4_jaZV-cRfwht6nmdbxxQPi7tVbT0tqWELf7HgFu6UqA3wxNL13sXhcNsu4K7Tvix60oWELQbZ1-jsPBQKDHqZDgs9L2mh_HboCAOokfjbCNTrTnNnefotcUVHdBjLGKBAKk0BFEkIWy0O2HtihZ_AufsXk24dPOMBuWrEUJ-NHvEz9qDnOlG3VJ68JoXUUPd_L04qaNd2cuC2b_T5eZJvoHD2OYxjF2MYuVkyITiTv9C8ttsQz7v2Ynhwhpg-whE-qTfWnkOPIAGDFWDhmNXR_CNVqYvuu4kmArTm2YskmlI93G6MoTkt1DJM1dfZ8MiGR3Nk4dbvA4WgQe624Q3TJlqPKA7yB57IAlmcs6ADel3wObfmHEFtzOZKos6E1nwS3FSwBXzKqWd6KMX7QExMyAVHtM",
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              rIdCidades: c.value
            })
          }).then(e => e.json()).then(e => {
            o.value = e
          })
        }, 2e3))
      })
    },
    validateCityOfBirth = e => !(hasNumbers(a) || a.length <= 1),
    validateDateExpedition = e => {
      let t = new Date(e);
      if (isNaN(t.getTime())) return !1;
      let l = new Date;
      return t > l
    },
    validateIsNumber = e => /^\d+$/.test(e),
    validateRequired = e => /[A-zÀ-ÿ!-_]/g.test(e),
    validateDateBirthLoan = e => {
      var t = e.split("/");
      return !(1940 >= parseInt(t[2]) || parseInt(t[2]) > 2002) && /(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{4})$/.test(e)
    },
    validateMonthlyIncome = e => {
      let t = /^R\$\s((?:1[0-9]|0?[1-9])\d{0,2}(?:\.\d{3})*(?:,\d+)?)$/;
      return t.test(e)
    },
    validateInstallments = e => 0 !== e,
    validatePassword = e => /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[$*&@#])[0-9a-zA-Z$*&@#]{8,}$/.test(e),
    noValidation = e => !0,
    validateConfirmPassword = e => {
      let t = document.getElementById("strPassword");
      return e == t.value
    },
    listTypes = {
      nome: validateName,
      cpf: validateCpf,
      cnpj: validateCNPJ,
      cpfcnpj: validateCPFCNPJ,
      datanasc: validateBirthDate,
      email: validateEmail,
      cel: validateCel,
      tel: validateLandline,
      celreq: "",
      telreq: "",
      telreq: "",
      telcel: validateAllPhones,
      cep: validateCep,
      required: validateRequired,
      banks: "",
      "cidade-nascimento": validateCityOfBirth,
      numbers: validateIsNumber,
      datExpedicao: validateDateExpedition,
      datNascimentoEmprestimo: validateDateBirthLoan,
      "renda-mensal": validateMonthlyIncome,
      parcelas: e => 0 !== e,
      "confirma-senha": validateConfirmPassword,
      senha: validatePassword,
      null: e => !0
    },
    listTypesMask = {
      cpf: setMaskCPF,
      cnpj: setMaskCNPJ,
      cpfcnpj: setMaskCpfCNPJ,
      cel: setMaskPhone,
      tel: setMaskPhone,
      celreq: setMaskPhone,
      telreq: setMaskPhone,
      telreq: setMaskPhone,
      telcel: setMaskPhone,
      cep: setMaskCEP,
      numbers: setMaskNumbers,
      datNascimentoEmprestimo: "",
      "renda-mensal": setMaskCurrency
    },
    listMaxLength = {
      cpf: 14,
      cnpj: 18,
      cpfcnpj: 18,
      cel: 15,
      tel: 14,
      celreq: 15,
      telreq: 14,
      telreq: 14,
      telcel: 15,
      cep: 9
    };
</script>
