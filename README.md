## 💻 Sobre a lib

O **formUI** e uma lib de componentes dinamicos de formulario em projeto PHP.

### 🎲 Instalação
 * Na raiz do projeto crie um arquivo **composer.json**, com o seguinte conteudo:
 ```bash
    {
        "name": "cenario-dev/form-ui",
    }

 ```

* Abra o projeto no terminal e execute o comando:
```bash
    composer install
```
* Agora faça a instalação da lib. Para isso execute no terminal:
```bash
    composer require cenario-capital/form-ui
```

### 🛠️ Usar
Utilize o require para importar a lib em sua pagina.
```bash
    <?php
        require_once __DIR__.'./vendor/cenario-capital/form-ui/src/formUI.php'
    ?>
```

Agora basta utilizar seguindo a documentação do notion

Notion formUI [https://bitter-ozraraptor-23c.notion.site/FormUI-b76dee4990bc4767a26697ab57980f55]

### ❗Importante
* Adicione no ***.gitignore*** a pasta ***vendor***
```bash
    vendor/*
```

* Caso haje erro com a versão do composer, siga os passos do link abaixo, para atualizar para o composer2
Download Composer2 [https://getcomposer.org/download/]